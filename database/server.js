const jsonserver = require('json-server');
const server = jsonserver.create();

const router = jsonserver.router('./database/db.json');

const middlewares = jsonserver.defaults();

server.use(middlewares);

server.use((req,res,next) => {
    next()
})

server.use(router)

server.listen(3000,() => console.log("json server is started on port 3000"));